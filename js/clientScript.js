$(document).ready(function () {
  $(".navbar-toggler").click(function (e) {
    e.stopImmediatePropagation();
    $(this).toggleClass("open");
    $(this)
      .siblings(".navbar-collapse")
      .slideToggle(750);
  });

  var typeitInstance = new TypeIt('.recognized-text', {
    // strings: ['This is my string!'],
    speed: 50
  });

  // This is what you will use to update the text as you receive it from Artyom thingie
  // typeitInstance.delete().pause(500).type("YOUR STRING HERE");
  // Also you can change the speed with this
  // typeitInstance.options({speed: 100})
})